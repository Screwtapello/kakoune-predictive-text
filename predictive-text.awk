function sqlquote(text) {
    gsub(/'/, "''", text)
    return "'" text "'"
}

BEGIN {
    print "PRAGMA foreign_keys = ON;"
    print "BEGIN TRANSACTION;"
    # In order to get the foreign key value, we have to insert the source record
    # *last*, which means we have to defer foreign key enforcement
    # for this transaction.
    print "PRAGMA defer_foreign_keys = ON;"
    print "DELETE FROM sources WHERE path = " sqlquote(ARGV[1]) ";"

    split(ARGV[3], words, " ")
    if (length(words) >= 1) {
        print "INSERT INTO trigrams " \
            "(source_id, word1, word2, word3, count) " \
            "VALUES "
        print "    (-1, '', '', " sqlquote(words[1]) ", 1);"
    }
    if (length(words) >= 2) {
        print "INSERT INTO trigrams " \
            "(source_id, word1, word2, word3, count) " \
            "VALUES "
        print "    (-1, '', " sqlquote(words[1]) ", " sqlquote(words[2]) ", 1);"
    }
    if (length(words) >= 3) {
        for (i=3; i<=length(words); i++) {
            trigrams[words[i-2] " " words[i-1] " " words[i]] += 1
        }
        for (each in trigrams) {
            count = trigrams[each]
            split(each, curr_words, " ")

            print "INSERT INTO trigrams " \
                "(source_id, word1, word2, word3, count) " \
                "VALUES"
            print "    (" \
                "-1, " \
                sqlquote(curr_words[1]) ", " \
                sqlquote(curr_words[2]) ", " \
                sqlquote(curr_words[3]) ", " \
                count \
                ");"
        }
    }

    print "INSERT INTO sources (path, filetype) VALUES"
    print "    (" sqlquote(ARGV[1]) "," sqlquote(ARGV[2]) ");"
    print "UPDATE trigrams SET source_id=last_insert_rowid()"
    print "    WHERE source_id=-1;"
    print "COMMIT TRANSACTION;"
}
