# Check for dependencies
evaluate-commands %sh{
    if ! command -V awk >/dev/null 2>/dev/null ; then
        echo 'fail awk not found in $PATH'
    fi
    if ! command -V sqlite3 >/dev/null 2>/dev/null ; then
        echo 'fail sqlite3 not found in $PATH'
    fi
}

declare-option -hidden str predictive_text_source %sh{dirname "$kak_source"}

# An option to control where predictive data are saved.
declare-option \
    -docstring "Where the predictive-text plugin stores its data" \
    str \
    predictive_text_path \
%sh{
    printf "%s\n" "${XDG_DATA_HOME:-$HOME/.local/share/}/kak/predictive-text/"
}

define-command predictive-text-db-reset \
    -hidden \
    -docstring "Reset the predictive-text database." \
%{
    nop %sh{
        mkdir -p "$kak_opt_predictive_text_path"
        rm -f "$kak_opt_predictive_text_path"/data.db
        sqlite3 "$kak_opt_predictive_text_path"/data.db "
            CREATE TABLE metadata AS
            SELECT 1 as version;

            CREATE TABLE sources
                ( source_id INTEGER PRIMARY KEY
                , path TEXT UNIQUE NOT NULL
                , filetype TEXT NOT NULL
                );

            CREATE TABLE trigrams
                ( source_id INTEGER REFERENCES sources ON DELETE CASCADE
                , word1 TEXT NOT NULL
                , word2 TEXT NOT NULL
                , word3 TEXT NOT NULL
                , count INTEGER
                , PRIMARY KEY (source_id, word1, word2, word3)
                );
        "
    }
}

define-command predictive-text-db-ensure \
    -docstring "Ensure the predictive-text database is available" \
%{
    evaluate-commands %sh{
        if ! [ -f "$kak_opt_predictive_text_path"/data.db ]; then
            printf "predictive-text-db-reset"
        fi
    }

    evaluate-commands %sh{
        version=$(sqlite3 -list "$kak_opt_predictive_text_path"/data.db \
            "SELECT version FROM metadata;")

        if [ 1 = "$version" ]; then
            sqlite3 "$kak_opt_predictive_text_path"/data.db "
                BEGIN;
                CREATE INDEX preceding_word ON trigrams(word2);
                UPDATE metadata SET version=2;
                COMMIT;
            "
            version=2
        fi
    }
}

define-command predictive-text-db-train-buffer \
    -docstring "Train the predictive text database with the current buffer" \
%{
    predictive-text-db-ensure

    evaluate-commands -draft -save-regs a %{
        # Verify we're actually in a buffer.
        nop %val{buffile}

        try %{
            # Make a regexp that matches all Kakoune's word chars.
            reg a %sh{
                # Alphabetic characters and digits are always word chars.
                printf %s 'A-Za-z0-9'

                eval "set -- $kak_quoted_opt_extra_word_chars"
                # If the option is empty, we default to underscore.
                [ "$#" -eq 0 ] && set -- _

                # We can't just put a dash in the character class
                # for fear of it forming a range,
                # so if we find any, we'll put a dash at the very end
                # where it can't form a range with anything.
                have_dash=0

                for char; do
                    case char in
                        -)  have_dash=1 ;;
                        ])  printf %s '\]' ;;
                        \\) printf %s '\\' ;;
                        *)  printf %s "$char" ;;
                    esac
                done

                [ "$have_dash" = "1" ] && printf %s '-'
            }

            # If this buffer has words in it...
            execute-keys <percent>s[ %reg{a} ]+|[^\s %reg{a} ]+<ret>

            # ...then add those words to the database,
            # replacing any previous words from this buffer.
            nop %sh{
                awk -f "$kak_opt_predictive_text_source"/predictive-text.awk \
                    "$kak_buffile" \
                    "$kak_opt_filetype" \
                    "$kak_selections" |
                    sqlite3 "$kak_opt_predictive_text_path"/data.db
            }
        } catch %{
            # This buffer does not have any words in it!
            # Remove any record of this buffer from the database.
            nop %sh{
                q() { printf "'%s'" "$( printf %s "$*" | sed "s/'/''/g")"; }
                sqlite3 "$kak_opt_predictive_text_path"/data.db "
                    PRAGMA foreign_keys = ON;
                    DELETE FROM sources WHERE path = $(q "$kak_buffile");
                "
            }
        }
    }
}

declare-option -hidden completions predictive_text_completions
set-option global completers option=predictive_text_completions %opt{completers}

define-command -hidden predictive-text-generate-completions -params 3 %{
    predictive-text-db-ensure

    evaluate-commands %sh{
        coords=$1
        word1=$2
        word2=$3

        q() { printf "'%s'" "$( printf %s "$*" | sed "s/'/''/g")"; }

        #echo echo -debug coords: "$coords" words: "$(q "$word1")" "$(q "$word2")"

        printf 'set-option window predictive_text_completions %s ' "$coords"

        sqlite3 -list "$kak_opt_predictive_text_path"/data.db "
            SELECT quote(
                replace(replace(word3, '\\', '\\\\'), '|', '\\|') ||
                '||' ||
                replace(replace(word3, '\\', '\\\\'), '|', '\\|')
            ) as candidate
            FROM (
                SELECT word3,
                    sum(CASE word1
                        WHEN $(q "$word1") THEN count
                        ELSE count/2.0
                    END) as count
                FROM trigrams
                NATURAL JOIN sources
                WHERE word2=$(q "$word2")
                    AND filetype=$(q "$kak_opt_filetype")
                GROUP BY word3
            )
            GROUP BY word3
            ORDER BY count DESC
            ;
        " | tr '\n' ' '
        echo
    }
}


define-command predictive-text-enable \
    -docstring "Enable predictive text completions in the current window" \
%{

    # Train the predictive text db after every file write.
    hook -group predictive-text window BufWritePost .* %{
        predictive-text-db-train-buffer
    }

    # Predict from the database when inserting.
    hook -group predictive-text window InsertIdle .* %{
        evaluate-commands -draft -save-regs ab %{
            # Clear the a and b registers
            reg a
            reg b

            # Have we started typing the new word yet?
            try %{
                exec -draft <space>h<a-k>\S<ret>

                # We have! We need to select this word (so we know what to
                # replace), then register b is the word before that, and
                # register a is the word before *that*.
                execute-keys <space>h<a-i>w<a-semicolon>

                evaluate-commands -draft %{
                    try %{
                        execute-keys b_"by
                        try %{ execute-keys b_"ay }
                    }
                }

                predictive-text-generate-completions \
                    "%val{cursor_line}.%val{cursor_column}+%val{selection_length}@%val{timestamp}" \
                    %val{main_reg_a} \
                    %val{main_reg_b}

            # We have not started typing the new word yet, just a space.
            } catch %{
                # The cursor is fine where it is, but we need to set register b
                # to the previous word, and register a to the word before
                # *that*.
                evaluate-commands -draft %{
                    try %{
                        execute-keys b_"by
                        try %{ execute-keys b_"ay }
                    }
                }

                predictive-text-generate-completions \
                    "%val{cursor_line}.%val{cursor_column}@%val{timestamp}" \
                    %val{main_reg_a} \
                    %val{main_reg_b}
            }
        }
    }
}

define-command predictive-text-disable \
    -docstring "Disable predictive text completions in the current window." \
%{
    remove-hooks window predictive-text
}
