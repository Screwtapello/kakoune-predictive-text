Predictive text input for Kakoune
=================================

Modern phones provide predictive text input
to help you enter text more efficiently.
They store a huge database of written messages,
and when you start typing something that seems familiar,
they suggest the next word in the sequence.

Kakoune's built-in completion features
are excellent for completing code,
which is quite concise,
but they fall down a bit when it comes to completing prose:
it's impractical to load the entire English vocabulary
(or any other language)
as possible completions,
and even if you do then Kakoune will suggest those words
entirely without regard for likelihood,
completing "cornflakes and m"
with "magnets" and "mechanical" before it gets around to "milk".

This plugin prodives a predictive text system for Kakoune,
learning from documents you create
and predicting the most common phrases as you type,
in the hope that this will make writing prose in Kakoune
just a little bit more convenient.

Features
========

  - Predictive text input,
    based on the documents you edit.
  - Records and predicts each filetype independently,
    so you don't get code suggestions in prose files.

**PRIVACY WARNING**:
The predictive text database this plugin creates
records every word in every file you edit,
along with their the file's full path
and the contents of the 'filetype' option.
If you're not comfortable with that information being recorded
on a particular computer,
do not use this plugin on that computer.

Requirements
============

  - An implementation of the AWK programming language
    (tested with mawk and gawk).
  - The `sqlite3` command-line tool.

Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `predictive-text.kak`,
    `predictive-text.awk`
    and `predictive-text.asciidoc` inside
    the new autoload directory,
    such as by checking out this git repository:

        git clone \
            https://gitlab.com/Screwtapello/kakoune-predictive-text.git \
            ~/.config/kak/autoload/kakoune-predictive-text/

Usage
=====

After the plugin is installed,
start Kakoune and check the `*debug*` buffer.
If you see any message like these:

> awk not found in $PATH

> sqlite3 not found in $PATH

...then the plugin could not find the executables it requires.
Make sure you can run them from a terminal, and try again.

Once the plugin is loaded,
you should configure Kakoune
to call `predictive-text-enable`
for the filetypes where you want predictive text completions.
For example:

    hook global WinSetOption filetype=markdown %{
        predictive-text-enable
        hook window -once WinSetOption filetype=.* %{
            predictive-text-disable
        }
    }

TODO
====

  - Silently? do not train on buffers with no filetype.
  - find a better way to pick previous words that's not `b`,
    since `b` will select indent whitespace before moving to the previous line,
    and we don't want to complete that.
  - Add a predictive-text-db-untrain-buffer command to erase a buffer from
    the database.
  - Do we need better indexes to efficiently serve our prediction query?
